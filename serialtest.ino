

const int ledPin = 13; // the pin that the LED is attached to
int incomingByte, count = 0, mode, width, height, numberOfBytes;      // a variable to read incoming serial data into
byte* dataArr;
#include <LiquidCrystal.h>
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  // initialize serial communication:
  Serial.begin(9600);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  //lcd.print("hello, world!");
}

void loop() {
  // see if there's incoming serial data:
  if (Serial.available() > 0) {
    // read the oldest byte in the serial buffer:
    //lcd.setCursor(0, 1);
    incomingByte = Serial.read();
    if (count == 0)
    {
      mode = incomingByte;  //read mode
      //lcd.print("Receiving data");
      //lcd.print("M");
    }
    else
    {
      if (count == 1)
      {
        width = incomingByte; //width of the incoming array
        //lcd.print("W");
      }
      else 
      {
        if (count == 2)
        {
          height = incomingByte;
        }
        else
        {
          if (count == 3)
          {
            numberOfBytes = incomingByte<<8; //MSB of number of bits
          }
          else
          {
            if (count == 4)  //LSB
            {
              numberOfBytes += incomingByte; //LSB of number of bits
              dataArr = (byte*)malloc(sizeof(byte)*numberOfBytes);
            }
            else //data
            {
              if (count < numberOfBytes + 5)
              {
                dataArr[count-5] = incomingByte;
                //lcd.print("Collecting data");
                if (count == numberOfBytes + 4)
                {
                  //process bytes
                  //lcd.print("done");
                  if (mode==0)
                  {
                    encodeImage();
                  }
                  else
                  {
                    decodeImage();
                  }
                  //Done processing current bytestream. Ready for next one
                  count = 0;
                  free(dataArr);
                }
              }
            }
          }
        }
      }
    }
    count = count + 1;
  }
}

void decodeImage()
{
  byte newCharArr[8][8] = {
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
  };

  byte* decodedImg = (byte*)malloc(sizeof(byte)*width*height);
  byte temp = 0, changepos = 0, MSB, LSB, charID;
  int c = 0;
  for (int i=0; i<=numberOfBytes; i++)
  {
    for (int j=0; j<dataArr[i]; j++)
    {
      decodedImg[c] = (i+1)%2; c++;
    }
  }
  
  /*for (int colid = 0; colid < width; colid++)
  {
    charID = colid/5;
    for (int rowid = 0; rowid < height; rowid++)
    {
      temp = newCharArr[charID][rowid];
      changepos = colid%5;
      MSB = temp>>(changepos+1);
      LSB = temp & ((1<<changepos)-1);
      newCharArr[charID][rowid] = (MSB<<(changepos+1)) + (decodedImg[height*colid + rowid]<<changepos) + LSB;
    }
  }*/
  
  int sum = 0, krange;
  int maxCharID = (width/5)+(width%5==0?0:1);
  for (int charID = 0; charID < maxCharID; charID++)
  {
    for (int rowid = 0; rowid < height; rowid++)
    {
      /*n0 = decodedImg[(charID*5)*height + rowid];
      n1 = decodedImg[(1+charID*5)*height + rowid];
      n2 = decodedImg[(2+charID*5)*height + rowid];
      n3 = decodedImg[(3+charID*5)*height + rowid];
      n4 = decodedImg[(4+charID*5)*height + rowid];*/
      
      if (charID < maxCharID-1)
        krange = 5;
      else
        krange = width-(maxCharID-1)*5;
      for (int k=0; k<krange; k++)
      {
        sum += (decodedImg[(k+charID*5)*height + rowid] * (1<<(4-k)));
      }
      newCharArr[charID][rowid] = sum;
      sum = 0;
    }
  }
  
  for (int i=0; i<8; i++)
  {
    lcd.createChar(byte(i), newCharArr[i]);
    lcd.setCursor(i,0);
    lcd.write(byte(i));
  }
}

void encodeImage()
{
  byte* binArr = (byte*)malloc(sizeof(byte)*width*height);
  byte curr = 0;
  int cc = 0, mrange = 0;
  for (int k=0; k<numberOfBytes; k++)
  {
    curr = dataArr[k];
    if (height>8)  //not relevant now that we can only display 8x40 images. so height always <=8
    {
      if (k%2 == 0)
        mrange = 8;
      else
        mrange = height-8;
    }
    else
    {
      mrange = height;
    }
    for (int m=0; m<mrange; m++)
    {
      binArr[cc] = curr%2;
      curr = curr>>1;
      cc += 1;
    }
  }
  int prev_state = 1;
  int c = 0, rle_c = 0;
  byte* rle_array = (byte*)malloc(sizeof(byte)*((width*height)+1));
  for (int n=0; n<width*height; n++)
  {
    if (binArr[n] != prev_state)
    {
      //add to rle_array
      if (c < 256)
      {
        rle_array[rle_c] = c; rle_c++;
      }
      else
      {
        rle_array[rle_c] = 255;
        rle_array[rle_c+1] = 0;
        rle_array[rle_c+2] = c-255;
        rle_c += 3;
      }
      c = 0;
    }
    c += 1;
    prev_state = binArr[n];
  }
  if (c < 256)
  {
    rle_array[rle_c] = c; rle_c++;
  }
  else
  {
    rle_array[rle_c] = 255;
    rle_array[rle_c+1] = 0;
    rle_array[rle_c+2] = c-255;
    rle_c += 3;
  }

  for (int n=0; n<rle_c; n++)
  {
    lcd.print(" ");
    lcd.print(rle_array[n]);
    if (n == 5)
      lcd.setCursor(0,1);
  }
}

