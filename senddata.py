import serial, time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

def rgb2gray(rgb):
	x = np.rint(255*np.dot(rgb[...,:3], [0.299, 0.587, 0.114]))
	return x.astype(int)

def readImage(imgname):
	img = mpimg.imread(imgname)   
	gray = rgb2gray(img)  
	plt.imshow(gray)
	#print gray
	#plt.show()
	binimage = (gray>250)+0
	print binimage
	return binimage, binimage.shape[1], binimage.shape[0]

def serialdata(binimage):
	imgdata = []
	for col in range(binimage.shape[1]):
		for row in range(binimage.shape[0]):
			if row%8!=0:
				sum += binimage[row][col]*(2**(row%8))
			else:
				sum = binimage[row][col]
			
			if row%8 == 7:
				#print imgdata
				imgdata.append(sum)
		if row%8 != 7:
			#print imgdata
			imgdata.append(sum)
	#print imgdata
	return imgdata

def splitnum(num):
	return [num>>8, num%256]

def sendSeialdata(data, mode, width, height):
	ser = serial.Serial('COM3', 9600, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE)
	#print splitnum(len(data)), len(data)
	values = [mode, int(width), int(height)] + splitnum(len(data)) + data  #add a header with mode, width, height and 2 bytes that show number of bytes in the data portion.
	print values, len(values)
	#print values
	time.sleep(5)
	return ser.write(values)

def breakdown(count):
	if count < 256:
		return [count]
	else:
		return [255,0,count-255]

def rle(binimage):
	binimage_1D = np.reshape(binimage.T, binimage.size)
	#print binimage_1D
	#return binimage_1D
	rle_array = []
	prev_state = 1
	count = 0
	for i in range(len(binimage_1D)):
		if binimage_1D[i] != prev_state:
			rle_array = rle_array + breakdown(count)
			count = 0
		count += 1
		prev_state = binimage_1D[i]
	rle_array = rle_array + breakdown(count)
				
	#print rle_array, len(rle_array)
	return (rle_array)

def operation(imgname, mode):
	binimage, width, height = readImage(imgname)
	#print binimage
	#print binimage.shape
	data = rle(binimage) if mode == 1 else serialdata(binimage)
	sendSeialdata(data,mode,width,height)
	#time.sleep(5)



operation('SineGoldSide.png', 1)
time.sleep(10)
operation("square_smaller.png", 0)
