# README #


### What is this repository for? ###

* Run Length Encoding based binary image codec

### How to run the code###

* senddata.py is the file that sends an image or its encoding to Arduino. This is python code that runs on the laptop/host
* serialtest.ino is the file which encodes or decodes the image and displays the output on an LCD screen. This runs on the arduino
* square_smaller.png, SineGoldSide.png - images used for testing the code

### Detailed Analysis ###
For more details check out below file under src:

* Report.pdf documents the details of the project